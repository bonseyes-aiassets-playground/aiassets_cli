import os
import pytest
import subprocess


def test_init_whole_body_pose_bad_credentials():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task whole_body_pose \
            --platform x86_64 \
            --environment cpu \
            --version v1.0 \
            --user gitlab+wrong-token-000000 \
            --password bad_password""",
        shell=True)
    assert rc == 1

    rc = subprocess.call("docker stop whole_body_pose", shell=True)
    assert rc == 1


def test_init_whole_body_pose_cpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task whole_body_pose \
            --platform x86_64 \
            --environment cpu \
            --version v1.0 \
            --user gitlab+deploy-token-557315 \
            --password AskgZQwcDRRYv3Da7BNB""",
        shell=True)
    assert rc == 0


def test_demo_image_pytorch_whole_body_pose_cpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine pytorch \
            --backbone shufflenetv2k30 \
            --image-input tests/samples/demo_image_whole_body_pose.jpg \
            --input-size 120x120 \
            --device cpu""",
        shell=True)
    assert rc == 0

    rc = subprocess.call("docker stop whole_body_pose", shell=True)
    assert rc == 0


def test_init_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli init \
            --task whole_body_pose \
            --platform x86_64 \
            --environment cuda10.2_tensorrt7.0 \
            --version v1.0 \
            --user gitlab+deploy-token-557315 \
            --password AskgZQwcDRRYv3Da7BNB""",
        shell=True)
    assert rc == 0


def test_export_all_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli export \
            --export-input-sizes 120x120 \
            --precisions fp16 fp32 \
            --engine all \
            --backbone shufflenetv2k30""",
        shell=True)
    assert rc == 0


def test_optimize_all_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli optimize \
            --optimize-input-sizes 120x120 \
            --engine all \
            --backbone shufflenetv2k30""",
        shell=True)
    assert rc == 0


def test_demo_image_pytorch_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine pytorch \
            --backbone shufflenetv2k30 \
            --image-input tests/samples/demo_image_whole_body_pose.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_image_onnx_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine onnxruntime \
            --backbone shufflenetv2k30 \
            --image-input tests/samples/demo_image_whole_body_pose.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_image_tensorrt_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo image \
            --engine tensorrt \
            --backbone shufflenetv2k30 \
            --image-input tests/samples/demo_image_whole_body_pose.jpg \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_demo_video_tensorrt_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli demo video \
            --engine tensorrt \
            --backbone shufflenetv2k30 \
            --video-input tests/samples/demo_video_whole_body_pose.mp4 \
            --input-size 120x120 \
            --device gpu""",
        shell=True)
    assert rc == 0


def test_benchmark_all_whole_body_pose_gpu():
    rc = subprocess.call(
        """export DEBUG=True && \
           bonseyes_aiassets_cli benchmark \
            --benchmark-input-sizes 120x120 \
            --dataset wholebody \
            --device gpu \
            --engine all \
            --backbone shufflenetv2k30""",
        shell=True)
    assert rc == 0


def test_shutdown_whole_body_pose_gpu():
    rc = subprocess.call("docker stop whole_body_pose", shell=True)
    assert rc == 0
