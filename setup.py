from setuptools import setup, find_packages

setup(
    name='bonseyes_aiassets_cli',
    version='1.0.1',
    url='https://gitlab.com/bonseyes-opensource/aiassets_cli',
    description='Bonseyes AI Asset Command Line Interface (CLI)',
    author='Vladimir Mujagic, Nikola Milojevic',
    author_email='vladimir@darwindigital.com, nikola.milojevic@darwindigital.com',
    packages=find_packages(),
    scripts=['bin/bonseyes_aiassets_cli'],
    install_requires=[
        'yaspin == 2.0.0',
        'docker == 5.0.2',
        'psutil == 5.8.0',
        'pytest == 6.2.5',
    ],
    python_requires='>=3.6',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
    ]
)
