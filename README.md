# Bonseyes AI Assets Command Line Interface (CLI)

This Python based Bonseyes AI Asset CLI tool lets you initialize and run AI Assets for a variety of tasks such as processing inputs from camera, videos, and audio and image files as well as carry out benchmarking tasks.

## Installation

1. Install from remote
```shell
pip3 install git+https://gitlab.com/bonseyes-opensource/aiassets_cli.git   
```
2. Add user path to system path
```shell
export PATH=$PATH:/home/${USER}/.local/bin
```

## Usage
To obtain access tokens visit: [https://www.bonseyes.com/](https://www.bonseyes.com/)        
For usage refer to official [documentation](https://bonseyes-opensource.gitlab.io/aiassets_cli/).
