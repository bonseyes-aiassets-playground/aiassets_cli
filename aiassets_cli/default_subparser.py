import argparse


def add_default_subparser(subparser):
    # DEMO
    demo_parser = subparser.add_parser('demo', help="AI Asset demo.")
    demo_subparser = demo_parser.add_subparsers(help='demo_commands', dest='demo_commands')
    demo_arguments = argparse.ArgumentParser(add_help=False)
    demo_arguments.add_argument('--input-size', '-is', required=True, type=str,
                                help='Model input size in format: WIDTHxHEIGHT')
    demo_arguments.add_argument('--engine', '-en', required=True, default='pytorch',
                                choices=['pytorch', 'onnxruntime', 'tensorrt'],
                                help='Inference engine options: pytorch | onnxruntime | tensorrt',
                                )
    demo_arguments.add_argument('--backbone', '-bb', required=False,
                                choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22', 'shufflenetv2k30',
                                         'shufflenetv2k16'],
                                help='Backbone options: mobilenetv1 | mobilenetv0.5 | resnet22 | shufflenetv2k30 | shufflenetv2k16'
                                )
    demo_arguments.add_argument('--precision', '-pr', required=False, default='fp32',
                                choices=['fp32', 'fp16', 'int8'],
                                help='Precision options: fp32 | fp16 | int8',
                                )
    demo_arguments.add_argument('--device', '-de', required=False, default='cpu',
                                choices=['gpu', 'cpu'],
                                help='Device options: gpu | cpu ',
                                )
    demo_arguments.add_argument('--cpu-num', '-cn', required=False, default=None, type=int,
                                help='Number of cpu cores: e.g 12',
                                )
    demo_arguments.add_argument('--thread-num', '-tn', required=False, default=None, type=int,
                                help='Number of threads: e.g 12',
                                )
    demo_arguments.add_argument("--thickness", '-th', required=False,
                                help="Thickness of the visualization overlay in pixels."
                                )
    demo_arguments.add_argument('--render', '-re', required=False, default='2d_sparse',
                                choices=['2d_sparse', '2d_dense', '3d', 'pose', 'axis'],
                                help='Rendering landmarks: 2d_sparse, 2d_dense, 3d, pose',
                                )
    demo_arguments.add_argument("--width", '-wi', required=False, help="Width in pixels.")
    demo_arguments.add_argument("--height", '-he', required=False, help="Height in pixels.")
    demo_arguments.add_argument("--color", '-co', required=False, help="Color format: GRAY | BGR.")
    demo_arguments.add_argument('--rotate', '-ro', required=False, type=int, choices=[90, -90, 180],
                                help='Rotation options: 90 | -90 | 180',
                                )

    # IMAGE
    image_args = demo_subparser.add_parser('image', parents=[demo_arguments], help="AI Asset demo image.")
    image_args.add_argument("--image-input", '-ii', required=True, help="Image path to process.")

    # VIDEO
    video_args = demo_subparser.add_parser('video', parents=[demo_arguments], help="AI Asset demo video.")
    video_args.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    video_args.add_argument("--video-input", '-vi', required=True, help="Video path to process.")

    # CAMERA
    camera_args = demo_subparser.add_parser('camera', parents=[demo_arguments], help="AI Asset demo camera.")
    camera_args.add_argument(
        '--single-face-track',
        '-sft',
        required=False,
        default=False,
        dest='single_face_track',
        action='store_true',
        help="Track single face.",
    )
    camera_args.add_argument(
        '--camera-id',
        '-ci',
        required=False,
        default=0,
        help="Camera ID",
    )

    # SERVER
    server_parser = subparser.add_parser('server', help="AI Asset server.")
    server_subparser = server_parser.add_subparsers(help='server_commands', dest='server_commands')
    server_arguments = argparse.ArgumentParser(add_help=False)

    # SERVER START
    server_start_arguments = server_subparser.add_parser('start', parents=[server_arguments],
                                                         help="AI Asset server start.")
    server_start_arguments.add_argument('--input-size', '-is', required=True, type=str,
                                        help='Model input size in format: WIDTHxHEIGHT')
    server_start_arguments.add_argument('--engine', '-en', required=True, default='pytorch',
                                        choices=['pytorch', 'onnxruntime', 'tensorrt'],
                                        help='Inference engine options: pytorch | onnxruntime | tensorrt',
                                        )
    server_start_arguments.add_argument('--backbone', '-bb', required=False,
                                        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22', 'shufflenetv2k30',
                                                 'shufflenetv2k16'],
                                        help='Backbone options: mobilenetv1 | mobilenetv0.5 | resnet22 | shufflenetv2k30 | shufflenetv2k16'
                                        )
    server_start_arguments.add_argument('--precision', '-pr', required=False, default='fp32',
                                        choices=['fp32', 'fp16', 'INT8'],
                                        help='Precision options: fp32 | fp16 | int8',
                                        )
    server_start_arguments.add_argument('--device', '-de', required=False, default='cpu',
                                        choices=['gpu', 'cpu'],
                                        help='Device options: gpu | cpu ',
                                        )
    server_start_arguments.add_argument('--cpu-num', '-cn', required=False, default=None, type=int,
                                        help='Number of cpu cores: e.g 12',
                                        )
    server_start_arguments.add_argument('--thread-num', '-tn', required=False, default=None, type=int,
                                        help='Number of threads: e.g 12',
                                        )

    # SERVER STOP
    server_stop_arguments = server_subparser.add_parser('stop', parents=[server_arguments],
                                                        help="AI Asset server stop.")

    # EXPORT
    export_args = subparser.add_parser('export', help="AI Asset model export.")
    export_args.add_argument("--export-input-sizes", '-eis', nargs='+', type=str, required=True,
                             help='Specify target input sizes: w1xh1 w2xh2 ...'
                             )
    export_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    export_args.add_argument(
        '--backbone',
        '-bb',
        required=False,
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22', 'shufflenetv2k30', 'shufflenetv2k16'],
        help='Backbone options: mobilenetv1 | mobilenetv0.5 | resnet22 | shufflenetv2k30 | shufflenetv2k16'
    )
    export_args.add_argument("--precisions", '-pcs', nargs='+', type=str, required=True, help="Export precisions")
    export_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    export_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    export_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # OPTIMIZE
    optimize_args = subparser.add_parser('optimize', help="AI Asset model optimization.")
    optimize_args.add_argument("--optimize-input-sizes", '-ois', nargs='+', type=str, required=True,
                               help='Specify target input sizes: w1xh1 w2xh2 ...'
                               )
    optimize_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    optimize_args.add_argument(
        '--backbone',
        '-bb',
        required=False,
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22', 'shufflenetv2k30', 'shufflenetv2k16'],
        help='Backbone options: mobilenetv1 | mobilenetv0.5 | resnet22 | shufflenetv2k30 | shufflenetv2k16'
    )
    optimize_args.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    optimize_args.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    optimize_args.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )

    # BENCHMARK
    benchmark_args = subparser.add_parser('benchmark', help="AI Asset benchmark.")
    benchmark_args.add_argument("--benchmark-input-sizes", '-bis', nargs='+', type=str, required=True,
                                help="Benchmark input sizes: w1xh1 w2xh2 ..."
                                )
    benchmark_args.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['pytorch', 'onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: pytorch | onnxruntime | tensorrt | all',
    )
    benchmark_args.add_argument(
        '--backbone',
        '-bb',
        required=False,
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22', 'shufflenetv2k30', 'shufflenetv2k16'],
        help='Backbone options: mobilenetv1 | mobilenetv0.5 | resnet22 | shufflenetv2k30 | shufflenetv2k16'
    )
    benchmark_args.add_argument(
        '--device',
        '-de',
        required=False,
        default='cpu',
        choices=['gpu', 'cpu'],
        help='Device options: gpu | cpu ',
    )
    benchmark_args.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=False,
        default='all',
        type=str,
        choices=['aflw', 'aflw2000-3d', 'wholebody', 'all'],
        help='Available devices: aflw | aflw2000-3d | all',
    )

    # TRAIN
    train_parser = subparser.add_parser('train', help="AI Asset model train.")
    train_subparser = train_parser.add_subparsers(help='train_commands', dest='train_commands')
    train_args = argparse.ArgumentParser(add_help=False)

    # TRAIN START
    train_start_args = train_subparser.add_parser('start', parents=[train_args], help="AI Asset train start.")
    train_start_args.add_argument(
        '--config',
        '-cfg',
        required=True,
        type=str,
        choices=[
            'v1.0_shufflenetv2k30_default_641x641_fp32_config',
            'v1.0_shufflenetv2k16_default_641x641_fp32_config',
            'pdc_default',
            'vdc_default',
            'wpdc_default'
        ],
        help='Config file name.',
    )
    # TRAIN STOP
    train_stop_args = train_subparser.add_parser('stop', parents=[train_args], help="AI Asset train stop.")
