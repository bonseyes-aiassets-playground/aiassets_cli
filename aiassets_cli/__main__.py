import subprocess
import argparse
import docker
import json
import os
import sys
import psutil
import re

from dd_parser import add_dd_subparser
from six_dof_parser import add_6dof_subparser
from sp_parser import add_sp_subparser
from wbp_parser import add_wbp_subparser
from fl_parser import add_fl_subparser


from random import randint
from yaspin import yaspin

DEBUG = os.getenv('DEBUG', 'False') == 'True'


def get_free_port():
    port = randint(49152, 65535)
    ports_in_use = []
    while True:
        conns = psutil.net_connections()
        for conn in conns:
            ports_in_use.append(conn.laddr[1])
        if port in ports_in_use:
            port = randint(49152, 65535)
        else:
            break
    return port


def get_active_aiasset():
    with open(os.path.expanduser("~/.bonseyes_aiassets"), "r") as bonseyes_conf:
        conf = json.load(bonseyes_conf)

    return conf['active_aiasset']


def get_aiasset_port(aiasset_name):
    with open(os.path.expanduser("~/.bonseyes_aiassets"), "r") as bonseyes_conf:
        conf = json.load(bonseyes_conf)

    return conf[f'{aiasset_name}_port']


def set_active_aiasset(aiasset_name):
    try:
        with open(os.path.expanduser("~/.bonseyes_aiassets"), "r") as bonseyes_conf:
            aiasset_conf = json.load(bonseyes_conf)
    except:
        aiasset_conf = dict()

    aiasset_conf['active_aiasset'] = aiasset_name
    with open(os.path.expanduser("~/.bonseyes_aiassets"), "w") as bonseyes_conf:
        json.dump(aiasset_conf, bonseyes_conf, indent=2)


def set_aiasset_port(aiasset_name, port_number):
    try:
        with open(os.path.expanduser("~/.bonseyes_aiassets"), "r") as bonseyes_conf:
            aiasset_conf = json.load(bonseyes_conf)
    except:
        aiasset_conf = dict()

    aiasset_conf[f'{aiasset_name}_port'] = port_number
    with open(os.path.expanduser("~/.bonseyes_aiassets"), "w") as bonseyes_conf:
        json.dump(aiasset_conf, bonseyes_conf, indent=2)


def cli():
    parser = argparse.ArgumentParser(description="Bonseyes AI Asset CLI Tool")
    subparser = parser.add_subparsers(help='CLI commands', dest='commands')

    # ACTIVE
    active_args = subparser.add_parser("active", help="Display active AI Asset")

    # USE
    use_args = subparser.add_parser('use', help="Select active AI Asset")
    use_args.add_argument("--task", '-t', required=True,
                          choices=['defect_detection', '6dof_object_detection', 'signal_processing', '3dface_landmarks',
                                   'whole_body_pose'],
                          help="Specify active AI Asset by task"
                          )

    if get_active_aiasset() == 'defect_detection':
        add_dd_subparser(subparser)
    elif get_active_aiasset() == '6dof_object_detection':
        add_6dof_subparser(subparser)
    elif get_active_aiasset() == 'signal_processing':
        add_sp_subparser(subparser)
    elif get_active_aiasset() == '3dface_landmarks':
        add_fl_subparser(subparser)
    elif get_active_aiasset() == 'whole_body_pose':
        add_wbp_subparser(subparser)

    return parser


def main():
    args = cli().parse_args()
    devnull = open(os.devnull, 'w')
    client = docker.from_env()

    try:

        # USE
        if args.commands == 'use':
            task_name = args.task
            port_number = get_free_port()
            set_active_aiasset(task_name)
            set_aiasset_port(get_active_aiasset(), port_number)

        elif args.commands == 'active':
            print("Active asset:", get_active_aiasset())
            print("Running on port:", get_aiasset_port(get_active_aiasset()))

        # DEMO
        elif args.commands == 'demo':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')

            # CAMERA
            if args.demo_commands == 'camera':
                cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
                thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''

                render = f"--render {args.render}" if args.render else ''
                thickness = f"--thickness {args.thickness}" if args.thickness else ''
                input_size = f"--input-size {args.input_size}" if 'input_size' in args else ''
                backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''

                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"python /app/interface/camera_processor.py {input_size} --camera-id {args.camera_id} --engine {args.engine} --precision {args.precision} {backbone} --device {args.device} {cpu_num} {thread_num} {render} {thickness} {'--single-face-track' if args.single_face_track else ''}",
                        demux=True)
                    if exit_code != 0:
                        if DEBUG:
                            print(err_output)
                            print(output)
                        raise Exception('Camera processing failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')

                try:
                    subprocess.check_output(f"docker cp {container_name}:/app/recording.mp4 .", shell=True)
                except subprocess.CalledProcessError as e:
                    raise Exception('Camera processing result saving failed!')

                try:
                    subprocess.check_call(f"docker cp {container_name}:/app/recording_predictions.json .", shell=True)
                except subprocess.CalledProcessError as e:
                    raise Exception('Camera processing result saving failed!')

                try:
                    subprocess.check_call(f"docker cp {container_name}:/app/recording_predictions.csv .", shell=True)
                except subprocess.CalledProcessError as e:
                    raise Exception('Camera processing result saving failed!')

                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"rm -rf recording.mp4 recording_predictions.json recording_predictions.csv", demux=True)
                    if err_output is not None:
                        raise Exception('Camera processing result deletion failed!')
                except docker.errors.APIError as e:
                    raise Exception('Camera processing result deletion failed!')

            # VIDEO
            elif args.demo_commands == 'video':
                video_name = args.video_input.split('/')[-1]
                video_name_json = video_name.replace('.mp4', '.json')
                video_name_csv = video_name.replace('.mp4', '.csv')

                if args.color == 'BGR':
                    color = ''
                elif args.color == 'GRAY':
                    color = '-ctg'
                else:
                    color = ''

                cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
                thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''

                input_size = f"--input-size {args.input_size}" if 'input_size' in args else ''
                backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''
                width = f"--width {args.width}" if args.width else ''
                height = f"--height {args.height}" if args.height else ''
                rotate = f"--rotate {args.rotate}" if args.rotate else ''
                render = f"--render {args.render}" if 'render' in args and args.render is not None else ''
                thickness = f"--thickness {args.thickness}" if 'thickness' in args and args.thickness is not None else ''

                with yaspin(text="Processing video(s): %s" % args.video_input, color="cyan") as sp:
                    try:
                        subprocess.check_output(f"docker cp {args.video_input} {container_name}:/app/", shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Input file path incorrect!')

                    try:
                        exit_code, (output, err_output) = container.exec_run(
                            f"python /app/interface/video_processor.py --input-size {args.input_size} --engine {args.engine} --precision {args.precision} --backbone {args.backbone} --device {args.device} {cpu_num} {thread_num} {rotate} {color} {width} {height} {render} {thickness} --video-input {video_name} {'--single-face-track' if args.single_face_track else ''}",
                            demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Video processing failed!')
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

                    try:
                        subprocess.check_output(f"docker cp {container_name}:/app/processed_{video_name} .", shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Video processing result saving failed!')

                    try:
                        subprocess.check_call(f"docker cp {container_name}:/app/processed_{video_name_json} .",
                                              shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Video processing result saving failed!')

                    try:
                        subprocess.check_call(f"docker cp {container_name}:/app/processed_{video_name_csv} .",
                                              shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Video processing result saving failed!')

                    try:
                        exit_code, (output, err_output) = container.exec_run(
                            f"rm -rf processed_{video_name} {video_name} processed_{video_name_json} processed_{video_name_csv}",
                            demux=True)
                        if err_output is not None:
                            raise Exception('Video processing result deletion failed!')
                    except docker.errors.APIError as e:
                        raise Exception('Video processing result deletion failed!')

            # IMAGE
            elif args.demo_commands == 'image':
                image_name = args.image_input.split('/')[-1]

                cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
                thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''

                render = f"--render {args.render}" if 'render' in args and args.render is not None else ''
                thickness = f"--thickness {args.thickness}" if 'thickness' in args and args.thickness is not None else ''
                input_size = f"--input-size {args.input_size}" if 'input_size' in args else ''
                backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''

                with yaspin(text="Processing image(s): %s" % args.image_input, color="cyan") as sp:
                    try:
                        subprocess.check_output(f"docker cp {args.image_input} {container_name}:/app/", shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Input file path incorrect!')

                    try:
                        exit_code, (output, err_output) = container.exec_run(
                            f"python /app/interface/image_processor.py {input_size} --engine {args.engine} --precision {args.precision} {backbone} --device {args.device} --jpg-input {image_name} {cpu_num} {thread_num} {render} {thickness}",
                            demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Image processing failed!')
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

                    try:
                        subprocess.check_output(f"docker cp {container_name}:/app/processed_{image_name} .", shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Image processing result saving failed!')

                    try:
                        subprocess.check_call(
                            f"docker cp {container_name}:/app/processed_{image_name.split('.')[0]}.json .", shell=True)
                    except subprocess.CalledProcessError as e:
                        raise Exception('Image processing result saving failed!')

                    try:
                        exit_code, (output, err_output) = container.exec_run(
                            f"rm -rf {image_name} processed_{image_name} processed_{image_name.split('.')[0]}.json",
                            demux=True)
                        if err_output is not None:
                            raise Exception('Image processing result deletion failed!')
                    except docker.errors.APIError as e:
                        raise Exception('Image processing result deletion failed!')
            # SIGNAL
            elif args.demo_commands == 'signal':
                csv_name = args.csv_input.split('/')[-1]

                try:
                    subprocess.check_output(f"docker cp {args.csv_input} {container_name}:/app/", shell=True)
                except subprocess.CalledProcessError as e:
                    raise Exception('Input file path incorrect!')

                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"python3 /app/interface/signal_processor.py --engine {args.engine} --precision {args.precision} --device {args.device} --csv-input {csv_name}",
                        demux=True)
                    result = output.decode("utf-8")
                    result = result.replace("\n", "")
                    json_pattern = r'\{.*\}'
                    data = re.search(json_pattern, result).group()
                    parsed_data = json.loads(data)
                    output_name = csv_name.split('.')[0] + '_processed.json'
                    with open(output_name, 'w') as json_file:
                        json.dump(parsed_data, json_file, indent=4)

                    if exit_code != 0:
                        if DEBUG:
                            print(err_output)
                            print(output)
                        raise Exception('CSV processing failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')



        # SERVER
        elif args.commands == 'server':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')
            if args.server_commands == 'start':
                cpu_num = f"--cpu-num {args.cpu_num}" if args.cpu_num else ''
                thread_num = f"--thread-num {args.thread_num}" if args.thread_num else ''
                input_size = f"--input-size {args.input_size}" if 'input_size' in args else ''
                backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''

                port_number = get_aiasset_port(container_name)

                print("Starting server on port number", port_number)
                try:
                    exit_code, output = container.exec_run(
                        f"python /app/interface/server.py {input_size} --engine {args.engine} --precision {args.precision} {backbone} --device {args.device} {cpu_num} {thread_num} --port {port_number} ",
                        detach=True)
                    # exit_code is None when detach=True
                    # if exit_code != 0:
                    #     if DEBUG:
                    #         print(err_output)
                    #         print(output)
                    #     raise Exception('Start server failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')

            elif args.server_commands == 'stop':
                with yaspin(text="Stopping server", color="cyan") as sp:
                    try:
                        exit_code, (output, err_output) = container.exec_run("cat /app/aiasset_info/server_pid",
                                                                             demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Stop server failed - unable to read process id!')

                        pid_server = [int(s) for s in output.split() if s.isdigit()][0]
                        exit_code, (output, err_output) = container.exec_run(f"kill -TERM -- {pid_server}", demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Stop server failed!')

                        exit_code, (output, err_output) = container.exec_run("rm /app/aiasset_info/server_pid",
                                                                             demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Stop server failed - unable to remove config file!')
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

        # EXPORT
        elif args.commands == 'export':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')

            input_size = f"--input-size {' '.join(args.input_size)}" if 'input_size' in args else f"--input-sizes {' '.join(args.export_input_sizes)}" if 'export_input_sizes' in args else ''
            backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''
            engine = ' '.join(args.engine)
            precisions = ' '.join(args.precisions)
            enable_dla = f'--enable-dla' if args.enable_dla else ''

            with yaspin(text="Exporting models, this might take a while", color="cyan") as sp:
                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"python /app/interface/exporter.py {input_size} --engine {engine} {backbone} --precisions {precisions} --workspace-unit {args.workspace_unit} --workspace-size {args.workspace_size} {enable_dla}",
                        demux=True)
                    if exit_code != 0:
                        if DEBUG:
                            print(err_output)
                            print(output)
                        raise Exception('Exporting failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')

        # OPTIMIZE
        elif args.commands == 'optimize':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')

            input_size = f"--input-size {' '.join(args.input_size)}" if 'input_size' in args else f"--input-sizes {' '.join(args.optimize_input_sizes)}" if 'optimize_input_sizes' in args else ''
            backbone = f"--backbone {args.backbone}" if 'backbone' in args and args.backbone is not None else ''
            engine = ' '.join(args.engine)
            enable_dla = f'--enable-dla' if args.enable_dla else ''

            with yaspin(text="Optimizing models, this might take a while", color="cyan") as sp:
                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"python /app/interface/optimizer.py {input_size} --engine {engine} {backbone} --workspace-unit {args.workspace_unit} --workspace-size {args.workspace_size} {enable_dla}",
                        demux=True)
                    if exit_code != 0:
                        if DEBUG:
                            print(err_output)
                            print(output)
                        raise Exception('Optimizing failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')

        # BENCHMARK
        elif args.commands == 'benchmark':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')

            input_size = f"--input-size {args.input_size}" if 'input_size' in args else ''
            precisions = f"--precisions {args.precisions}" if 'precisions' in args else ''
            engine = ' '.join(args.engine)

            with yaspin(text="Performing benchmark, this might take a while", color="cyan") as sp:
                try:
                    exit_code, (output, err_output) = container.exec_run(
                        f"python /app/interface/benchmark.py {input_size} --dataset {args.dataset} --engine {engine} {precisions} --backbone {args.backbone} --device {args.device}",
                        demux=True)
                    if exit_code != 0:
                        if DEBUG:
                            print(err_output)
                            print(output)
                        raise Exception('Benchmark failed!')
                except docker.errors.APIError as e:
                    raise Exception('Server failure!')

            try:
                subprocess.check_output(f"docker cp {get_active_aiasset()}:/app/eval_results/benchmark.json .",
                                        shell=True)
            except subprocess.CalledProcessError as e:
                raise Exception('Benchmark result saving failed!')

            try:
                subprocess.check_output(f"docker cp {get_active_aiasset()}:/app/eval_results/benchmark.csv .",
                                        shell=True)
            except subprocess.CalledProcessError as e:
                raise Exception('Benchmark result saving failed!')

        # TRAIN
        elif args.commands == 'train':
            container_name = get_active_aiasset()
            try:
                container = client.containers.get(container_name)
            except docker.errors.APIError as e:
                raise Exception('AIAsset has not been properly initialized!')

            if args.train_commands == 'start':
                with yaspin(text="Start training a model, this might take a while", color="cyan") as sp:
                    try:
                        exit_code, (output, err_output) = container.exec_run(
                            f"python /app/interface/trainer.py --config {args.config}", demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Start training failed!')
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

            elif args.train_commands == 'status':
                with yaspin(text="Retrieving training status", color="cyan") as sp:
                    try:
                        exit_code, (output, err_output) = container.exec_run(f"tail /app/aiasset_info/train_log -n 1",
                                                                             demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Training status retrieval failed!')
                        else:
                            print('\nCurrent training status is:')
                            print(output)
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

            elif args.train_commands == 'stop':
                with yaspin(text="Stopping training", color="cyan") as sp:
                    try:
                        exit_code, (output, err_output) = container.exec_run("cat /app/aiasset_info/train_pid",
                                                                             demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Stop training failed - unable to read process id!')

                        train_pid = [int(s) for s in output.split() if s.isdigit()][0]
                        exit_code, (output, err_output) = container.exec_run(f"kill -TERM -- -{train_pid}", demux=True)
                        if exit_code != 0:
                            if DEBUG:
                                print(err_output)
                                print(output)
                            raise Exception('Stop training failed!')

                        exit_code, (output, err_output) = container.exec_run(
                            "rm /app/aiasset_info/train_pid /app/aiasset_info/train_log", demux=True)
                        # if exit_code != 0:
                        #     if DEBUG:
                        #         print(err_output)
                        #         print(output)
                        #     raise Exception('Stop training failed - unable to remove config file!')
                    except docker.errors.APIError as e:
                        raise Exception('Server failure!')

    except Exception as e:
        print(e)
        sys.exit(1)


if __name__ == '__main__':
    main()
