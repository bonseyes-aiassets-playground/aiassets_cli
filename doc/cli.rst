AI Asset CLI
------------
AI Asset CLI provides an command line interface for manipulation with the Bonseyes AI Assets.

Available Bonseyes AI Assets:

1. 3D Face Landmarks
2. Bodypose Detection


Commands
--------

.. automodule:: aiassets_cli.__main__
    :members:

.. argparse::
   :ref: aiassets_cli.__main__.cli
   :prog: aiassets_cli